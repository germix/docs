# Comandos Git

## Resetear 'credential helper'

```bash
git config --system --unset credential.helper
```


## Listar ramas

```
git branch
```

## Crear rama

```
git branch <nombre de rama>
```

## Cambiar de rama

```
git checkout <nombre de rama>
```

## Eliminar rama

```
git branch -d <nombre de rama>
```

## Unir con rama

```
Estar en master y luego
git merge <nombre de rama>
```

## Pushear una rama

```
git push origin <nombre de rama>
```

## Pushear todas las ramas

```
git push origin --all
```

## Listar branches no mergeados

```
git branch --no-merged master
```

## Eliminar branch en remoto

```
git push origin --delete <nombre de rama>
```

## Retrack untracked file

```
git update-index --no-assume-unchanged filename
```

## Eliminar un commit aun cuando ya fue subido al repositorio

```
git reset <commit> --hard
git push -f origin master           // Se necesita permisos
```

## Guardado rápido provisional

```
git stash
```
https://git-scm.com/book/es/v1/Las-herramientas-de-Git-Guardado-r%C3%A1pido-provisional

